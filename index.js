require("dotenv").config();
const fs = require("fs");

const LENGTH = Number(process.env.LENGTH) || Math.floor(Math.random() * 24) + 8;

let result = ";";
for (let i = 0; i < LENGTH; i++) {
  result += String.fromCharCode(Math.floor(Math.random() * 105) + 21);
}

fs.writeFileSync(
  "./output.json",
  JSON.stringify({
    length: LENGTH,
    result,
  })
);
